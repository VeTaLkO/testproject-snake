﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(IntVector2))]

public class IntVector2Drawer : PropertyDrawer 
{
	public override void OnGUI (Rect position, SerializedProperty property, GUIContent label) 
	{
		Rect contentPosition = EditorGUI.PrefixLabel(position, label);
		contentPosition.width *= 0.3f;
		EditorGUI.indentLevel = 0;
		EditorGUIUtility.labelWidth = 18f;
		EditorGUI.PropertyField(contentPosition, property.FindPropertyRelative("x"), new GUIContent(" X"));

		contentPosition.x += contentPosition.width;
		//contentPosition.width *= 0.5f;
		//EditorGUIUtility.labelWidth = 14f;
		EditorGUI.PropertyField(contentPosition, property.FindPropertyRelative("y"), new GUIContent(" Y"));
	}
}