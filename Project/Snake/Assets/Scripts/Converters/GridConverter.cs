﻿using UnityEngine;
using System.Collections;

public class GridConverter : Singleton<GridConverter>
{
	public IntVector2 GridSizeInTiles { get; private set;}

	private IntVector2 extraPadding; // = new IntVector2(100, 100); // 100*100 as extra padding

	private Vector2 gameZoneSizeInPixels;
	private float tileSize;



	public void Setup(IntVector2 gridSizeInTiles)
	{
		GridSizeInTiles = gridSizeInTiles;

		var screenWidth = /*Camera.main.pixelWidth; */ tk2dCamera.Instance.nativeResolutionWidth; 
		var screenHeight = /*Camera.main.pixelHeight; */ tk2dCamera.Instance.nativeResolutionHeight; 

		extraPadding = new IntVector2 ((int)(screenWidth * 0.02f), (int)(screenHeight * 0.02f));

		gameZoneSizeInPixels.x = screenWidth - extraPadding.x; 
		var tileSizeX = gameZoneSizeInPixels.x / GridSizeInTiles.x;

		gameZoneSizeInPixels.y = screenHeight - extraPadding.y;  
		var tileSizeY = gameZoneSizeInPixels.y / GridSizeInTiles.y;

		tileSize = Mathf.Min (tileSizeX, tileSizeY);
		Debug.LogFormat("Tile X shoud be {0} in order to fit {1} tiles in {2} pixels in width (scren width is {3})", 
			tileSizeX, GridSizeInTiles.x, gameZoneSizeInPixels.x, screenWidth );
		Debug.LogFormat("Tile Y shoud be {0} in order to fit {1} tiles in {2} pixels in height (screen height is {3})", 
			tileSizeY, GridSizeInTiles.y, gameZoneSizeInPixels.y, screenHeight );
	}


	public Vector3 GetTileSize()
	{
		float scale = tileSize / 64; // 64 is sprite size
		return new Vector3(scale, scale);
	}


	public Vector3 GridToScreen(int x, int y)
	{
		return GridToScreen(new IntVector2(x, y));
	}

	public Vector3 GridToScreen(IntVector2 gridPos)
	{
		var result = new Vector3 ();

		// Calculating from left bottom corner, taking into account distance between tiles
		result.x = (gridPos.x - GridSizeInTiles.x / 2 + 0.5f) * tileSize /* + (gridPos.x - GridSizeInTiles.x / 2 ) * 1.1f*/; 
		result.y = (gridPos.y - GridSizeInTiles.y / 2 + 0.5f) * tileSize /*+ (gridPos.y - GridSizeInTiles.y / 2 ) * 1.1f*/; 

//		Debug.Log ("Converting grid " + gridPos + " to world (" + result.x + "," + result.y + ")");	
		return result;
	}


}
