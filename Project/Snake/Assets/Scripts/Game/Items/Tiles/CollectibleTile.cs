﻿using DG.Tweening;
using System.Collections;
using UnityEngine;


public class CollectibleTile : BaseTile 
{
	private Sequence moveAtSeq;

	void Start ()
	{
		transform.localScale = 0.8f * GridConverter.Instance.GetTileSize ();

		moveAtSeq = DOTween.Sequence ();
		moveAtSeq
			.Append (transform.DOPunchScale (new Vector3 (0.3f, 0.3f, 0), 0.15f, 5, 1f))
			.SetAutoKill (false).SetDelay (2).SetLoops (-1); //.Pause();
	}
}
