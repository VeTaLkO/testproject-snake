﻿using Gamelogic;
using UnityEngine;
using System.Collections;


public abstract class BaseTile : BaseSprite
{
	public IntVector2 GridPosition {get; protected set;} // NOTE: for debug, comment out accessors so Unity would be able to show property in inspector

	public void PlaceAtGridPosition(IntVector2 pos)
	{
		GridPosition = pos;
		PlaceAtGridPosition ();
	}

	public void PlaceAtGridPosition()
	{
		var newPos = GridConverter.Instance.GridToScreen(GridPosition);
		gameObject.transform.SetXY (newPos.x, newPos.y);
	}
}
