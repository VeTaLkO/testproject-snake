﻿using DG.Tweening;
using System.Collections;
using UnityEngine;

public class SnakeTailTile : BaseTile
{
	public bool ToBeDestroyed = false;

	private Sequence moveAtSeq;

	void Start ()
	{
		transform.localScale = GridConverter.Instance.GetTileSize ();

		/*
		moveAtSeq = DOTween.Sequence ();
		moveAtSeq
			.Append(DOTween.ToAlpha (() => TkSprite.color, x => TkSprite.color = x, 0, 0.3f))
			//.Append(transform.DOScale(new Vector3(0.2f,0.2f), 0.2f).From(true))
			.SetAutoKill(false).Pause()
			.OnComplete(OnDestroyCompleteCallback);
			*/
	}

	public void AnimateAndDestroy()
	{
		ToBeDestroyed = true;
		//moveAtSeq.Restart ();

		TkSprite.DOFade (0, 0.2f).OnComplete (OnDestroyCompleteCallback);

		//OnDestroyCompleteCallback();
	}

	private void OnDestroyCompleteCallback()
	{
		gameObject.SetActive(false);
		GameObject.Destroy(gameObject);
	}
}
