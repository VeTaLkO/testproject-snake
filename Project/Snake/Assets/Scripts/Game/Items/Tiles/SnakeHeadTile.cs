﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeHeadTile : BaseTile, IHandle<SwipeMessage>
{
	private const float StartSnakeSpeed = 3.5f; // Start speed: 3.5 blocks per sec

	private float snakeSpeed;
	private float snakeNextMovementTime = 0;
	private SwipeDirection lastDirection;

	private Queue<SnakeTailTile> taleQueue = new Queue<SnakeTailTile>();

	private float musicPitch = 1f;

	private Sequence moveAtSeq;


	public override void Awake()
	{		
		MessengerSingleton.Messenger.Subscribe(this); // Either subscribe directly or inherit MonoBehaviourEx. Not much sence to inherit BaseSprite from MonoBehaviourEx otherwise every sprite would be a subscriber.
		base.Awake();
	}

	void Start ()
	{
		snakeSpeed = StartSnakeSpeed; 
		transform.localScale = GridConverter.Instance.GetTileSize ();

		var res = StorageManager.Instance.GetSnakeDirection ();
		lastDirection = (res == SwipeDirection.None)
			? SwipeDirection.Right
			: res;

		moveAtSeq = DOTween.Sequence ();
		moveAtSeq.Append(transform.DOScale(new Vector3(0.2f,0.2f), 0.2f).From(true)).SetAutoKill(false);
		
		//var tale = new List<IntVector2> { new IntVector2 (1, 1), new IntVector2 (9, 9) };
		//var result = StorageManager.Instance.GetSnakeTail();

		var tale = StorageManager.Instance.GetSnakeTail (); // Tail will be reconstructed if it was saved
		foreach (var item in tale) 
			AddTalePiece (item);		
	}

	void Update()
	{
		if (!gameObject.activeSelf)
			return;
		
		if (Time.time >= snakeNextMovementTime) 
		{
			Move ();
			snakeNextMovementTime = Time.time + 1/snakeSpeed; // time = dist/speed.
		}
	}

	void OnApplicationQuit() 
	{
		StorageManager.Instance.SetSnakeDirection (lastDirection);
		StorageManager.Instance.SetSnakeTail (taleQueue.Where(t => !t.ToBeDestroyed).Select(p => p.GridPosition).ToList()); // Save tale's GridPositions
	}



	public void Handle(SwipeMessage message)
	{
		Debug.Log (message.SwipeDirection);

		if (message.SwipeDirection == SwipeDirection.None) // If new gesture is invalid, just leave
			return;

		// Don't allow snake to catch itself too easy
		if (Constants.ForbiddenDirectionsDictionary [message.SwipeDirection] == lastDirection) // Leave, gesture is forbidden
			return;
		
		lastDirection = message.SwipeDirection;
	}


	private void Move()
	{
		var currentPos = GridPosition;
		var nextPos = currentPos + Constants.DirectionsDictionary [lastDirection];
		var caughtFood = false;

		//SoundManager.Instance.PlaySoundOnce (Constants.SoundMove);

		// Checking background
		var backgroundTile = GameController.Instance.GetTileAt (nextPos, true); 
		if (backgroundTile == null)
			throw new UnityException (string.Format ("There's no background tile at {0}, extremely suspisious", nextPos));
		if (string.Compare (backgroundTile.TileName, TilesName.Dark.ToString ()) == 0) 
		{
			GameOver ();
			return;
		}

		// Checking active space (foreground)
		var foregroundTile = GameController.Instance.GetTileAt (nextPos, false); 
		if (foregroundTile != null)
		{
			if (string.Compare (foregroundTile.TileName, TilesName.Light.ToString ()) == 0) { // Caught food
				MessengerSingleton.Messenger.Publish (new EatFoodMessage (foregroundTile));

				SoundManager.Instance.PlayRandomSoundOnce (Constants.EatSounds);

				musicPitch += 0.003f;
				SoundManager.Instance.SetMusicPitch (musicPitch);

				snakeSpeed += 0.15f;
				caughtFood = true;
			}
		}

		// Checking the tale
		if (IsTaleAt (nextPos)) {
			GameOver ();
			return;
		}

		// If no gameover, proceed with movement
		PlaceAtGridPosition (nextPos);
		moveAtSeq.Restart ();

		AddTalePiece (currentPos);

		if (!caughtFood)
			RemoveTalePiece ();
	}

	private void GameOver()
	{
		StorageManager.Instance.SetSnakeDirection (SwipeDirection.None);
		StorageManager.Instance.SetSnakeTail (new List<IntVector2> ());

		SoundManager.Instance.PlaySoundOnce (Constants.SoundLose);
		musicPitch = 1f;
		SoundManager.Instance.SetMusicPitch (musicPitch);

		MessengerSingleton.Messenger.Publish (new GameoverMessage ());

		DestroyTale ();

		this.gameObject.SetActive(false);
		GameObject.Destroy(this.gameObject);
	}


	private bool IsTaleAt(IntVector2 position)
	{
		return (taleQueue.FirstOrDefault (t => t.GridPosition.x == position.x && t.GridPosition.y == position.y) != null);			
	}

	private void AddTalePiece(IntVector2 position)
	{
		var newTale = Factory.Instantiate<SnakeTailTile> (Constants.PrefabItemSnakeTailTile);
		newTale.PlaceAtGridPosition (position);
		taleQueue.Enqueue (newTale);
	}

	private void RemoveTalePiece()
	{
		var oldTale = taleQueue.Dequeue ();
		oldTale.AnimateAndDestroy ();
	}

	private void DestroyTale()
	{
		foreach (var item in taleQueue) 
		{
			item.gameObject.SetActive (false);
			GameObject.Destroy(item.gameObject);
		}

		taleQueue.Clear ();
	}
}
