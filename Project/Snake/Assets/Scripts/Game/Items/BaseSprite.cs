﻿using UnityEngine;
using System.Collections;

public abstract class BaseSprite : MonoBehaviour // MonoBehaviourEx 
{		
	private string tileName;
	public string TileName 
	{
		get { return tileName; }
		set
		{
			tileName = value; 
			TkSprite.SetSprite (tileName);
		}
	}
	
	private tk2dSprite _sprite;
	public tk2dSprite TkSprite 
	{
		get
		{
			if (_sprite == null)
				_sprite = GetComponent<tk2dSprite>();
			if (_sprite == null)
			{
				//_sprite = gameObject.AddComponent<tk2dSprite>();
				// TODO: set SpriteCollection. However, if to instantiate a prefab, this case should never happen.
				Debug.LogError("Please, use prefabs (so far). tk2dSprite component is not found.");
			}
			
			return _sprite;
		}
		set { _sprite = value; }
	}
	
	
	public string SortingLayer
	{
		set { TkSprite.GetComponent<Renderer>().sortingLayerName = value; }
	}

	public int SortingOrderLayer
	{
		set { TkSprite.GetComponent<Renderer> ().sortingOrder = value; }
	}

	public virtual void Awake()
	{
		TileName = TkSprite.CurrentSprite.name;
	}
}
