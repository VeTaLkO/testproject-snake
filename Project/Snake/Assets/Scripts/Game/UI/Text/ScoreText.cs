﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ScoreText : BaseText
{
	private Sequence slideSeq;

	public void Start()
	{
//		slideSeq = DOTween.Sequence ();
//		slideSeq
//			.Append(transform.DOMoveY (.3f, 0.5f).SetRelative())
//			.Insert(0f, DOTween.ToAlpha (() => textMesh.color, x => textMesh.color = x, 0, 0.5f))
//			.SetAutoKill(false).Pause();
	}

	public void Show(string score)
	{
		textMesh.text = score;
		transform.localScale = new Vector3 (1, 1, 1);
		textMesh.color = new Color (1, 1, 1, 1);
		//slideSeq.Restart ();

		DOTween.ToAlpha (() => textMesh.color, x => textMesh.color = x, 0, 0.8f);
		transform.DOScale (new Vector2 (1.4f, 1.4f), 0.6f);
		transform.DOMoveY (.5f, 0.8f).SetRelative()
			.OnComplete(OnDestroyCompleteCallback);
	}

	private void OnDestroyCompleteCallback()
	{
		gameObject.SetActive(false);
	}
}
