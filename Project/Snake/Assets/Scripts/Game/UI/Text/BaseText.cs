﻿using UnityEngine;
using System.Collections;

public class BaseText : MonoBehaviour 
{	
	private tk2dTextMesh _textMesh;
	public tk2dTextMesh textMesh 
	{
		get
		{
			if (_textMesh == null)
				_textMesh = GetComponent<tk2dTextMesh>();
			if (_textMesh == null)
			{
				//_sprite = gameObject.AddComponent<tk2dSprite>();
				// TODO: set SpriteCollection. However, if to instantiate a prefab, this case should never happen.
				Debug.LogError("Please, use prefabs (so far). tk2dSprite component is not found.");
			}
			
			return _textMesh;
		}
		set { _textMesh = value; }
	}
}
