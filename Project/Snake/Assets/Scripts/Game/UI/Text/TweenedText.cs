﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class TweenedText : BaseText 
{
	public string Text 
	{ 
		set { textMesh.text = value; if (moveAtSeq != null) moveAtSeq.Restart();}
		get { return textMesh.text;}
	}

	private Sequence moveAtSeq;

	public void Start()
	{
		moveAtSeq = DOTween.Sequence ();
		moveAtSeq
			.Append(transform.DOPunchScale (new Vector3(35f,35f,0), 0.15f, 5, 0.1f))
			.SetAutoKill(false).Pause();
	}
}
