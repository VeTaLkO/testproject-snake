﻿using UnityEngine;
using System.Collections;

public class TotalScoreText : TweenedText, IHandle<EatFoodMessage>, IHandle<GameoverMessage>
{
	private int score;
	private int Score  // Should be public, but no need to show it to everybody
	{ 
		set { score = value; Text = score.ToString("00"); }
		get { return score; }
	}


	public void Awake()
	{
		MessengerSingleton.Messenger.Subscribe(this);

		Score = StorageManager.Instance.GetScore ();
	}


	public void Handle(EatFoodMessage message)
	{
		Score++;
		StorageManager.Instance.SetScore (Score);
	}

	public void Handle(GameoverMessage message)
	{
		Score = 0;
		StorageManager.Instance.SetScore (0);
	}
}
