﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameController : MonoSingleton<GameController>, IHandle<GameoverMessage>, IHandle<EatFoodMessage>, IHandle<CloseApplicationMessage>
{
	public IntVector2 GridSize;

	private IList<BaseTile> backgroundTiles = new List<BaseTile>();
	private IList<BaseTile> foregroundTiles = new List<BaseTile>();

	private SnakeHeadTile snakeHead;

	public override void Awake() // Overrides MonoBehaviourEx.Awake()
	{
		base.Awake ();
		GridConverter.Instance.Setup (GridSize);
		SoundManager.Instance.PlayMusic (Constants.MusicSnakeLoop);
	}

	void Start ()
	{
		LoadLevel ();
		SpawnFood (StorageManager.Instance.GetFoodPosition ());
		SpawnSnake (StorageManager.Instance.GetSnakeHeadPosition());
	}

	void OnApplicationQuit() 
	{
		StorageManager.Instance.SetFoodPosition (foregroundTiles.First().GridPosition);
		StorageManager.Instance.SetSnakeHeadPosition (snakeHead.GridPosition);
	}


	public void Handle(GameoverMessage message)
	{
		Cleanup ();
		Start();
	}

	public void Handle(EatFoodMessage message)
	{
		RemoveFood (message.Tile);
		SpawnFood ();
	}

	public void Handle(CloseApplicationMessage message)
	{
		Application.Quit ();
	}


	public BaseTile GetTileAt(IntVector2 position, bool isBackground) // isBackground: true = background, false = foreground
	{
		return isBackground 
			? backgroundTiles.First (t => t.GridPosition.x == position.x && t.GridPosition.y == position.y)
			: foregroundTiles.FirstOrDefault (t => t.GridPosition.x == position.x && t.GridPosition.y == position.y);
	}


	private void LoadLevel()
	{
		// TODO: load level

		for (int x = 0; x< GridSize.x; x++)
		{
			for (int y = 0; y< GridSize.y; y++)
			{
				var tile = Factory.Instantiate<BackgroundTile> (Constants.PrefabItemBackgroundTile);
				tile.PlaceAtGridPosition (new IntVector2(x, y));

				var middleCenterAnchor = tk2dCamera.Instance.transform.FindChild (Constants.AnchorMiddleCenter);
				if (middleCenterAnchor == null)
					throw new UnityException ("Child " + Constants.AnchorMiddleCenter + " is not found as main camera child");

				tile.transform.parent = middleCenterAnchor;
				tile.name = string.Format ("Tile_{0}x{1}", x, y);
				backgroundTiles.Add (tile);

				// Walls
				if (x == 0 || y == 0 || x == GridSize.x - 1 || y == GridSize.y - 1)
				{
					tile.TileName = TilesName.Dark.ToString (); // Use Dark sprite for walls
					tile.SortingOrderLayer = -1;
				}
				else
					// tile.TileName = Random.Range(0, 100) <= 50 ? TilesName.Mid.ToString () : TilesName.Light.ToString(); // Pick Mid or Light sprite randomly
					tile.TileName = TilesName.Mid.ToString ();
			}
		}
	}
		
	private void SpawnFood(IntVector2 position = null)
	{		
		var res = (position == null)
			? GetRandomEmptyCell () 
			: position;
		
		Debug.LogFormat ("Placing food at {0}", res);

		var tile = Factory.Instantiate<CollectibleTile> (Constants.PrefabItemCollectibleTile);
		tile.PlaceAtGridPosition (res);
		foregroundTiles.Add (tile);
	}

	private void RemoveFood(BaseTile tile)
	{
		tile.gameObject.SetActive (false);
		GameObject.Destroy (tile.gameObject);
		foregroundTiles.Remove (tile);
	}

	private void SpawnSnake(IntVector2 position = null)
	{
		var res = (position == null)
			? new IntVector2(GridSize.x/2, GridSize.y/2)
			: position;
		
		snakeHead = Factory.Instantiate<SnakeHeadTile> (Constants.PrefabItemSnakeHeadTile);
		snakeHead.PlaceAtGridPosition (res);
	}


	private IntVector2 GetRandomEmptyCell()
	{
		var result = new IntVector2 (0,0);
		var tileName = string.Empty;
		do 
		{
			result.x = Random.Range(0, GridSize.x); // Pick random point inside of the grid
			result.y = Random.Range(0, GridSize.y);

			var tile = GetTileAt(result, true);

			if (tile != null)
				tileName = tile.TileName;
				
		} while (string.IsNullOrEmpty(tileName) || string.Compare(tileName, TilesName.Dark.ToString()) == 0 ); // Try until we find an empty cell
		return result;
	}

	private void Cleanup()
	{
		StorageManager.Instance.SetFoodPosition (new IntVector2(0,0));
		StorageManager.Instance.SetSnakeHeadPosition (new IntVector2(0,0));

		foreach (var item in backgroundTiles.Union(foregroundTiles)) 
		{
			item.gameObject.SetActive (false);
			GameObject.Destroy (item.gameObject);
		}

		backgroundTiles.Clear ();
		foregroundTiles.Clear ();
	}
}
