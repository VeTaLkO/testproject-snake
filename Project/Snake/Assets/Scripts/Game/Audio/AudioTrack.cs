﻿using UnityEngine;
using System.Collections;

public class AudioTrack : MonoBehaviour 
{
	private AudioSource audioSource;

	public void PlaySoundOnce (AudioClip audioClip)
	{
		if (audioSource == null)
			audioSource = gameObject.AddComponent<AudioSource> ();

		StartCoroutine (PlaySoundCoroutine (audioClip));
	}

	IEnumerator PlaySoundCoroutine (AudioClip audioClip)
	{
		audioSource.PlayOneShot (audioClip);
		yield return new WaitForSeconds (audioClip.length);
		Destroy (gameObject);
	}
}
