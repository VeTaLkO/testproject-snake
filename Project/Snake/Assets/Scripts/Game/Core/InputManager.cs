﻿using UnityEngine;
using System.Collections;

/* // NOTE: defined in Unity
public enum TouchPhase
	{
		Began,
		Moved,
		Stationary,
		Ended,
		Canceled
	}
 */

// NOTE: swipes info is taken from here: http://forum.unity3d.com/threads/swipe-in-all-directions-touch-and-mouse.165416/

// NOTE: detect only tiles on the 10 layer
public class InputManager : MonoBehaviourEx
{		
	private TouchPhase phaseState;
	
	public bool Settings_AreDebugMessagesEnabled = true;
	public bool Settings_IsTappingEnabled = true;
	public bool Settings_IsSwipingEnabled = true;
	public float Settings_MinSwipeLength = 20f;
	
	//private const int layerGameTiles = 1 << 10;
	
	private Vector2 firstTouchPos;
	
	void Start()
	{
		Settings_AreDebugMessagesEnabled = false;
	}
	
	void Update()
	{
		phaseState = TouchPhase.Canceled;

		#if UNITY_IPHONE
		var touches = Input.touchCount;
		
		if (touches > 0)
		{
			if (Input.touches[0].phase == TouchPhase.Began)
			{
				phaseState = TouchPhase.Began;
				
				if (Settings_IsTappingEnabled)
					HandleTap(Input.touches[0].position);
				
				if (Settings_IsSwipingEnabled)
					HandleSwipe(Input.touches[0].position);
			}
			else if (Input.touches[0].phase == TouchPhase.Moved)
			{
				phaseState = TouchPhase.Moved;
				
				if (Settings_IsTappingEnabled)
					HandleTap(Input.touches[0].position);
				
				if (Settings_IsSwipingEnabled)
					HandleSwipe(Input.touches[0].position);
			}
			else if (Input.touches[0].phase == TouchPhase.Ended)
			{
				phaseState = TouchPhase.Ended;
				
				if (Settings_IsTappingEnabled)
					HandleTap(Input.touches[0].position);
				
				if (Settings_IsSwipingEnabled)
					HandleSwipe(Input.touches[0].position);
			}
		}
		#endif
		
		//#if UNITY_EDITOR
		// Keyboard input support

		if (Input.GetKeyDown (KeyCode.Escape))
			Messenger.Publish (new CloseApplicationMessage ());
		
		if (Settings_IsSwipingEnabled) 
		{
			if (Input.GetKeyDown (KeyCode.UpArrow))
				Messenger.Publish (new SwipeMessage (SwipeDirection.Up));
			if (Input.GetKeyDown (KeyCode.DownArrow))
				Messenger.Publish (new SwipeMessage (SwipeDirection.Down));
			if (Input.GetKeyDown (KeyCode.RightArrow))
				Messenger.Publish (new SwipeMessage (SwipeDirection.Right));
			if (Input.GetKeyDown (KeyCode.LeftArrow))
				Messenger.Publish (new SwipeMessage (SwipeDirection.Left));	
		}
		//#elif
		if (Input.GetMouseButtonDown(0))
		{			
			phaseState = TouchPhase.Began;
			
			if (Settings_IsTappingEnabled)
				HandleTap(Input.mousePosition);
			
			if (Settings_IsSwipingEnabled)
				HandleSwipe(Input.mousePosition);
		}
		else if (Input.GetMouseButton(0))
		{
			phaseState = TouchPhase.Moved;
			
			if (Settings_IsTappingEnabled)
				HandleTap(Input.mousePosition);
			
			if (Settings_IsSwipingEnabled)
				HandleSwipe(Input.mousePosition);
		}
		else if (Input.GetMouseButtonUp(0))
		{
			phaseState = TouchPhase.Ended;
			
			if (Settings_IsTappingEnabled)
				HandleTap(Input.mousePosition);
			
			if (Settings_IsSwipingEnabled)
				HandleSwipe(Input.mousePosition);
		}
		
		//#endif

		/*
		#if UNITY_WEBPLAYER
		if (Input.GetMouseButtonDown(0))
		{
			phase = PHASE_BEGIN;
			touches = 1;
		}
		else if (Input.GetMouseButton(0))
		{
			phase = PHASE_MOVE;
			touches = 1;
		}
		else if (Input.GetMouseButtonUp(0))
		{
			phase = PHASE_END;
			touches = 1;
		}
		#endif
		*/


	}
	
	void HandleTap(Vector3 _screenPosition)
	{
		/*
		if( Camera.main != null )
		{
			_screenPosition = Input.mousePosition;
			var _worldPosition 	= Camera.main.ScreenToWorldPoint( _screenPosition );
			RaycastHit2D[] raysHitArray = Physics2D.RaycastAll( _worldPosition, Vector2.zero, 30f, layerGameTiles );
			
			if (Settings_AreDebugMessagesEnabled)
				Debug.Log("Touch state: " + phaseState + " at " + _screenPosition + " / " + _worldPosition + " , found " + raysHitArray.Count());
			
			Messenger.Publish(new TouchMessage(phaseState, _screenPosition, _worldPosition, raysHitArray));
		}*/
	}
	
	void HandleSwipe(Vector2 _screenPosition)
	{
		if (phaseState == TouchPhase.Began)
			firstTouchPos = _screenPosition;
		
		if (phaseState == TouchPhase.Ended)
		{
			var currentPos = _screenPosition;
			var diff = currentPos - firstTouchPos;
			
			if (diff.magnitude < Settings_MinSwipeLength)
			{
				Messenger.Publish(new SwipeMessage(SwipeDirection.None));
				
				if (Settings_AreDebugMessagesEnabled)
					Debug.Log("Swipe detected: None. Too short: " + diff.magnitude + " limit is " + Settings_MinSwipeLength );
				
				return;
			}
			
			diff.Normalize();
			SwipeDirection swipeDirection = SwipeDirection.None;
			
			if (Mathf.Abs(diff.x) > Mathf.Abs(diff.y)) // if horizontal else vertical			
				swipeDirection = diff.x > 0 ? SwipeDirection.Right : SwipeDirection.Left;
			else
				swipeDirection = diff.y > 0 ? SwipeDirection.Up : SwipeDirection.Down;			
			
			if (Settings_AreDebugMessagesEnabled)
				Debug.Log("Swipe detected: " + swipeDirection);
			
			Messenger.Publish(new SwipeMessage(swipeDirection));
		}
	}
	
	void OnDrawGizmos()
	{/*
		Ray newRay 			= new Ray();
		newRay.direction 	= Vector3.forward;
		newRay.origin		= _WorldPosition;
		
		Gizmos.color = Color.green;
		Gizmos.DrawRay( newRay ); /* */
	}
}