﻿using CodeStage.AntiCheat.ObscuredTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class StorageManager : Singleton<StorageManager> 
{
	private const string FoodPosition = "FoodPosition";
	private const string SnakeHeadPosition = "SnakeHeadPosition";
	private const string SnakeDirectionPosition = "SnakeDirectionPosition";
	private const string SnakeTalePositions = "SnakeTalePositions";
	private const string Score = "Score";

	#region Food pos
	public void SetFoodPosition(IntVector2 position)
	{
		ObscuredPrefs.SetVector2 (FoodPosition, new Vector2 (position.x, position.y));
	}

	public IntVector2 GetFoodPosition()
	{
		var res = ObscuredPrefs.GetVector2 (FoodPosition);

		return res == Vector2.zero
			? null
			: new IntVector2 ((int)res.x, (int)res.y);
	}
	#endregion

	#region Snake Head
	public void SetSnakeHeadPosition(IntVector2 position)
	{
		ObscuredPrefs.SetVector2 (SnakeHeadPosition, new Vector2 (position.x, position.y));
	}

	public IntVector2 GetSnakeHeadPosition()
	{
		var res = ObscuredPrefs.GetVector2 (SnakeHeadPosition);

		return res == Vector2.zero
			? null
			: new IntVector2 ((int)res.x, (int)res.y);
	}
	#endregion

	#region Snake Direction
	public void SetSnakeDirection(SwipeDirection direction)
	{
		ObscuredPrefs.SetString (SnakeDirectionPosition, direction.ToString());
	}

	public SwipeDirection GetSnakeDirection()
	{
		var res = ObscuredPrefs.GetString (SnakeDirectionPosition);

		return (string.IsNullOrEmpty(res))
			? SwipeDirection.None
			: (SwipeDirection)Enum.Parse (typeof(SwipeDirection), res, true);
	}
	#endregion

	#region Snake Tail
	public void SetSnakeTail(IList<IntVector2> tale)
	{
		var serialized = new JSONObject (JSONObject.Type.ARRAY);
		foreach (var item in tale) 
			serialized.Add(string.Format("{0}:{1}", item.x, item.y));
		
		ObscuredPrefs.SetString (SnakeTalePositions, serialized.Print());
	}

	public List<IntVector2> GetSnakeTail()
	{
		var serialized = ObscuredPrefs.GetString (SnakeTalePositions);
		var j = new JSONObject (serialized);
		var result = new List<IntVector2> ();

		foreach (var item in j.list) 
			result.Add (new IntVector2 (int.Parse(item.str.Split (':').First ()), int.Parse(item.str.Split (':').Last ())));

		return result;
	}

	#endregion

	#region Score

	public void SetScore(int score)
	{
		ObscuredPrefs.SetInt (Score, score);
	}

	public int GetScore()
	{
		return ObscuredPrefs.GetInt (Score);
	}

	#endregion
}
