﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : MonoSingleton<SoundManager>
{
	private AudioSource musicPlayer;

	public override void Awake()
	{
		base.Awake ();
		musicPlayer = gameObject.AddComponent<AudioSource> ();
	}

	public void SetMusicPitch(float pitch)
	{
		musicPlayer.pitch = pitch;
	}

	public void PlayMusic(string path)
	{
		musicPlayer.clip = Resources.Load (path, typeof(AudioClip)) as AudioClip;
		musicPlayer.Stop ();
		musicPlayer.volume = 0.7f;
		musicPlayer.loop = true;
		musicPlayer.Play ();
	}

	public void PlaySoundOnce(string path)
	{
		var audio = new GameObject(path.Split('/').Last().ToLower(), typeof(AudioTrack) );
		audio.transform.parent = this.transform;
		var script = audio.GetComponent<AudioTrack> ();

		var clip = Resources.Load (path, typeof(AudioClip)) as AudioClip;
		script.PlaySoundOnce (clip);
	}

	public void PlayRandomSoundOnce(IList<string> paths)
	{
		PlaySoundOnce (paths [Random.Range (0, paths.Count)]);
	}
}
