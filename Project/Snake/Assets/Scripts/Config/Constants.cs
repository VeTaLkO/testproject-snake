﻿using System.Collections.Generic;

public enum TilesName
{
	Dark,
	Mid,
	Light,
	White
}

public static class Constants 
{
	public const string AnchorMiddleCenter = "Anchor (MiddleCenter)";

	public const string PrefabItemBackgroundTile = "Prefabs/Items/BackgroundTile";
	public const string PrefabItemCollectibleTile = "Prefabs/Items/CollectibleTile";
	public const string PrefabItemSnakeHeadTile = "Prefabs/Items/SnakeHeadTile";
	public const string PrefabItemSnakeTailTile = "Prefabs/Items/SnakeTailTile";

	public const string MusicSnakeLoop = "Audio/Music/snakeLoop";

	public const string SoundLose = "Audio/Sounds/lose";
	public const string SoundMove = "Audio/Sounds/move";

	public const string SoundEat1 = "Audio/Sounds/eat1";
	public const string SoundEat2 = "Audio/Sounds/eat2";
	public const string SoundEat3 = "Audio/Sounds/eat3";
	public const string SoundEat4 = "Audio/Sounds/eat4";
	public const string SoundEat5 = "Audio/Sounds/eat5";
	public const string SoundEat6 = "Audio/Sounds/eat6";

	public static IDictionary<SwipeDirection, IntVector2> DirectionsDictionary;
	public static IDictionary<SwipeDirection, SwipeDirection> ForbiddenDirectionsDictionary;
	public static IList<string> EatSounds;

	static Constants()
	{
		DirectionsDictionary = new Dictionary<SwipeDirection, IntVector2>() {
			{ SwipeDirection.Right, new IntVector2 (1, 0) },
			{ SwipeDirection.Left, new IntVector2(-1,0) },
			{ SwipeDirection.Up, new IntVector2(0,1) },
			{ SwipeDirection.Down, new IntVector2(0,-1) }
		};

		ForbiddenDirectionsDictionary = new Dictionary<SwipeDirection, SwipeDirection> () {
			{ SwipeDirection.Right, SwipeDirection.Left},
			{ SwipeDirection.Up, SwipeDirection.Down},
			{ SwipeDirection.Left, SwipeDirection.Right},
			{ SwipeDirection.Down, SwipeDirection.Up}
		};

		EatSounds = new List<string> () {
			SoundEat1,
			SoundEat2,
			SoundEat3,
			SoundEat4,
			SoundEat5,
			SoundEat6
		};
	}
}
