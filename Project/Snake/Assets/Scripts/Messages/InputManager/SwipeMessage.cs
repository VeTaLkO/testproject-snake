﻿using UnityEngine;
using System.Collections;

public enum SwipeDirection
{
	Up,
	Down,
	Left,
	Right,
	None	// NOTE: be careful, dont forget to handle this
}

public class SwipeMessage 
{
	public SwipeDirection SwipeDirection { get; set; }
	
	public SwipeMessage (SwipeDirection _swipeDirection)
	{
		SwipeDirection = _swipeDirection;
	}		
}
