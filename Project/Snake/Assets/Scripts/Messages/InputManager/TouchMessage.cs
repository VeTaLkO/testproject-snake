﻿using UnityEngine;
using System.Collections;

public class TouchMessage 
{
	public TouchPhase TouchPhase { get; set; }
	public Vector2 ScreenPosition { get ; set; }
	public Vector2 WorldPosition { get; set; }
	public RaycastHit2D[] RaysHitArray { get; set; }
	
	public TouchMessage() { }
	public TouchMessage(TouchPhase phase,  Vector2 _screenPos, Vector2 _worldPos, RaycastHit2D[] _raysHitArray)
	{
		TouchPhase = phase;
		ScreenPosition = _screenPos;
		WorldPosition = _worldPos;
		
		RaysHitArray = _raysHitArray;
	}
}
