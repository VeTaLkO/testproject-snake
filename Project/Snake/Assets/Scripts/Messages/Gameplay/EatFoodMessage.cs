﻿using UnityEngine;
using System.Collections;

public class EatFoodMessage 
{
	public BaseTile Tile { get; private set; }

	public EatFoodMessage(BaseTile tile)
	{
		Tile = tile;
	}
}
