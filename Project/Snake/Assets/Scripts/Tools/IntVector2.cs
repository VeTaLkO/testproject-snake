﻿using UnityEngine;
using System;
using System.Collections;

[Serializable]
public class IntVector2  
{
	public int x, y;
	
	public IntVector2 (int[] _xy) 
	{
		x = _xy[0];
		y = _xy[1];
	} 
	
	public IntVector2 (int _x, int _y) 
	{
		x = _x;
		y = _y;
	} 

	public IntVector2 (Vector2 vector) 
	{
		x = (int)vector.x;
		y = (int)vector.y;
	} 
	
	public int Length
	{
		get { return x * y; }
	}
	
	public override bool Equals (object obj)
	{
		if (obj is IntVector2)
		{
			var ivec = (IntVector2) obj;
			return (x == ivec.x && y == ivec.y);
		}
		return false;
	}
	
	public override string ToString ()
	{
		return string.Format ("[{0},{1}]", x, y);
	}
	
	public static bool operator ==(IntVector2 left, IntVector2 right)
	{
		// If both are null, or both are same instance, return true.
		if (ReferenceEquals(left, right))
		{
			return true;
		}
		
		// Return true if the fields match:
		return left.Equals(right);
	}
	
	public static bool operator !=(IntVector2 left, IntVector2 right)
	{
		return !(left == right);
	}

	public static IntVector2 operator +(IntVector2 left, IntVector2 right) 
	{
		return new IntVector2(left.x + right.x, left.y + right.y);
	}

	public static IntVector2 operator -(IntVector2 left, IntVector2 right) 
	{
		return new IntVector2(left.x - right.x, left.y - right.y);
	}

	public static IntVector2 operator *(IntVector2 left, int right) 
	{
		return new IntVector2(left.x * right, left.y * right);
	}
	
	public override int GetHashCode()
	{
		var hashCode = 0;
		
		hashCode ^= x.GetHashCode();
		hashCode ^= y.GetHashCode();
		
		return hashCode;
	}
}
