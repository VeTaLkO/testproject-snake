﻿using UnityEngine;
using System.Collections;


public static class Vector2Ex 
{
	public static IntVector2 ToIntVector2 (this Vector2 vector2) 
	{
		int[] intVector2 = new int[2];
		for (int i = 0; i < 2; ++i) 
			intVector2[i] = Mathf.RoundToInt(vector2[i]);
		return new IntVector2 (intVector2);
	}
	
	public static IntVector2 RotatePoint(this IntVector2 pointToRotate, IntVector2 centerPoint, float angleInDegrees)
	{
		var angleInRadians = angleInDegrees * (Mathf.PI / 180);
		var cosTheta = Mathf.Cos(angleInRadians);
		var sinTheta = Mathf.Sin(angleInRadians);
		return new IntVector2(
			(int) (cosTheta * (pointToRotate.x - centerPoint.x) - sinTheta * (pointToRotate.y - centerPoint.y) + centerPoint.x),
			(int) (sinTheta * (pointToRotate.x - centerPoint.x) + cosTheta * (pointToRotate.y - centerPoint.y) + centerPoint.y)
			);
	}
	
	public static float SqrDistanceTo(this IntVector2 startPos, IntVector2 endPos)
	{
		return Mathf.Pow (startPos.x - endPos.x, 2) + Mathf.Pow (startPos.y - endPos.y, 2);
	}
}