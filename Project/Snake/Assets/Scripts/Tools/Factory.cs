﻿using UnityEngine;
using System.Collections;

public static class Factory
{
	public static T Instantiate<T>(string resourcePath, Transform parent = null) where T: MonoBehaviour
	{
		try 
		{
			var newInstance = (GameObject)GameObject.Instantiate(Resources.Load(resourcePath));
			newInstance.name = resourcePath.Split('/').Last();
			
			if (parent != null)
				newInstance.transform.parent = parent;
			
			return newInstance.GetComponent(typeof(T)) as T;
		} 
		catch (System.Exception) 
		{
			Debug.LogError("Something went wrong at instancing. Resource path: " + resourcePath + ". Requested type: " + typeof(T));
			return null;
		}		
	}
	
	public static GameObject Instantiate(string resourcePath, Transform parent = null)
	{
		try 
		{
			var newInstance = (GameObject)GameObject.Instantiate(Resources.Load(resourcePath));
			newInstance.name = resourcePath.Split('/').Last();
			
			if (parent != null)
				newInstance.transform.parent = parent;
			
			return newInstance;
		} 
		catch (System.Exception) 
		{
			Debug.LogError("Something went wrong at instancing. Resource path: " + resourcePath);
			return null;
		}		
	}
	
	
	
	/*
	public static T InstantiateFromPool<T>(string resourcePath) where T: MonoBehaviour
	{
		try 
		{
			PoolingSystem ps = PoolingSystem.Instance;
			
			if (ps == null) 
				Debug.LogException(new MissingComponentException("Pool is null"));
			
			return ps.InstantiateAPS (resourcePath.Split('/').Last()).GetComponent(typeof(T)) as T;
		} 
		catch (System.Exception) 
		{
			Debug.LogError("Something went wrong at instancing. Resource path: " + resourcePath + ". Requested type: " + typeof(T));
			return null;
		}
	}
	
	public static GameObject InstantiateFromPool(string resourcePath)
	{
		try 
		{
			PoolingSystem ps = PoolingSystem.Instance;
			
			if (ps == null) 
				Debug.LogException(new MissingComponentException("Pool is null"));
			
			return ps.InstantiateAPS (resourcePath.Split('/').Last());
		} 
		catch (System.Exception) 
		{
			Debug.LogError("Something went wrong at instancing. Resource path: " + resourcePath);
			return null;
		}		
	}*/
}
